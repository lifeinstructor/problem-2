﻿using System;
using System.Collections.Generic;

namespace Problem_2
{
    public class Program
    {
        public static void Main()
        {
            Employee Joe = new Employee("Joe");
            Employee Harry = new Employee("Harry");
            Employee Chang = new Employee("Chang");
            Employee Mary = new Employee("Mary");
            Employee Alfonse = new Employee("Alfonse");

            addTeamMember(Joe, Harry);
            addTeamMember(Harry, Chang);
            addTeamMember(Harry, Mary);
            addTeamMember(Mary, Alfonse);

            Employee[] employees = new Employee[] { Joe, Harry, Chang, Mary, Alfonse };

            //Search for employees here
            string[] subOrdinates = getSubOrdinates(employees, "Mary");
            for (int i = 0; i < subOrdinates.Length; i++)
                Console.WriteLine(subOrdinates[i]);

            Console.ReadLine();
        }

        public static string[] getSubOrdinates(Employee[] employees, string employeeName)
        {
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i].name.ToLower() == employeeName.ToLower())
                {
                    List<Employee> subOrdinates = getTeamMembersOfEmployee(employees[i]);
                    string[] subOrdinatesResults = new string[subOrdinates.Count];
                    for (int j = 0; j < subOrdinates.Count; j++)
                    {
                        subOrdinatesResults[j] = subOrdinates[j].name;
                    }
                    return subOrdinatesResults;
                }
            }
            return new string[] { };
        }

        public static void addTeamMember(Employee employee, Employee teamMember)
        {
            employee.addTeamMember(teamMember);
        }

        public static List<Employee> getTeamMembersOfEmployee(Employee employee)
        {
            List<Employee> results = new List<Employee>();
            List<Employee> teamMembers = employee.team;

            if (teamMembers.Count > 0)
                results.AddRange(teamMembers);
            else
                return results;

            for (int i = 0; i < teamMembers.Count; i++)
            {
                results.AddRange(getTeamMembersOfEmployee(teamMembers[i]));
            }

            return results;
        }
    }

    public class Employee
    {
        private string _name;
        private List<Employee> _team;

        public Employee(string name)
        {
            this._name = name;
            this._team = new List<Employee>();
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public List<Employee> team
        {
            get { return _team; }
            set { _team = value; }
        }

        public void addTeamMember(Employee member)
        {
            _team.Add(member);
        }
    }
}
